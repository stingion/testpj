package com.crossengage;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class UserRepositoryTest {

    @Test
    public void test() throws IOException {
        UserRepository repository = new UserRepository(new File(this.getClass().getResource("/test_user_data.txt").getFile()));
        List<String> emails = repository.getAllEmails();

        assertThat(emails).hasSize(5);
        System.out.println(emails);
    }
}
